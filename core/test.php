<?php
	// This script is intended to be run from a local PHP dev server.
	//This file is in the root directory.
	$script_url = '/atbar/en/dev/atbar-analytics.js';
	if(array_key_exists('source', $_GET)) $script_url = "//$_GET[source]$script_url";
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ATBar Testing Range</title>
		<style>
			body { font-family: Helvetica, Arial, sans-serif}
			h1 { font-weight: 400 }
		</style>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics_debug.js','ga');
		
		  ga('create', 'UA-124512836-5', 'auto');
		  ga('send', 'pageview', '/options.html');
		
		</script>
	</head>
	<body>
		<h1>ATBar Testing Range</h1>
		<?php if(array_key_exists('source', $_GET)): ?>
			<a href="javascript:(function(){d=document;jf=d.createElement('script');jf.src='<?=$script_url?>';jf.type='text/javascript';jf.id='ToolBar';d.getElementsByTagName('head')[0].appendChild(jf);})();" title="Launch ATBar"><img src="https://core.atbar.org/resources/img/launcher.png" /></a>
		<?php endif; ?>
		<p><a href='test.php?source=localhost'>Use script from localhost</a></p>
		<p><a href='test.php?source=core.atbar.org'>Use script from core.atbar.org</a></p>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	</body>
</html>